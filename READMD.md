# 三海作品集


## 项目详情


### 代码结构
```
pmhub
├── pmhub-admin -- 核心配置，如：国际化、mybatis、日志、swagger及配置文件
├── pmhub-common -- 通用组件都放在这个模块，各模块公共方法、注解、配置、常量、模型转换、异常、过滤器，全局预防 xss 脚本攻击
├── pmhub-workflow -- 流程管理模块，包含流程分类、表单设计、流程设计、部署管理
├── pmhub-framework -- 关于框架相关功能都在这个模块，如多数据源配置，限流处理、MybatisPlus、redis、连接池配置等
├── pmhub-generator -- 代码生成相关控制器及配置
├── pmhub-oa -- 企微绑定以及第三方 OA 系统绑定，统一登录认证中心
├── pmhub-project -- 涉及项目管理，任务管理、项目设置，任务流转等
├── pmhub-quartz -- 定时任务调度中心
├── pmhub-system -- 对应系统管理模块，含用户管理、角色管理、日志管理等
├── pmhub-ui -- 前端项目源码
```